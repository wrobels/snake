import java.applet.Applet;
import java.awt.*;

public class Snake extends Applet {

    private int headX, headY;
    private int xSpeed, ySpeed;
    private int snakeLength;
    private Tail head;
    private Tail[] tail = new Tail[2000];

    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT;

        Direction() {
        }
    }

    private Direction direction;

    Snake() {
        snakeLength = 0;
        headX = 250;
        headY = 200;
        for (int i = 0; i <= 4; i++) {
            tail[i] = new Tail(headX - (10 * (i - 1)), headY);
            snakeLength = i;
        }

        head = tail[0];
        xSpeed = 10;
        ySpeed = 0;

        direction = Direction.RIGHT;

    }

    void setDirection(Direction value) {
        direction = value;
    }

    Direction getDirection() {
        return direction;
    }

    void draw(Graphics g) {

        g.setColor(Color.white);

        for (int i = 1; i <= snakeLength; i++) {
            if (tail[i] != null) {
                g.fillRect(tail[i].getX() - 10, tail[i].getY() - 10, 10, 10);
            }
        }

        for (int i = snakeLength; i >= 0; i--) {
            if (snakeLength > 0 && tail[i + 1] != null) {
                tail[i + 1].setTail(tail[i].getX(), tail[i].getY());
            }
        }

        g.setColor(Color.GREEN);
        g.fillRect(head.getX() - 10, head.getY() - 10, 10, 10);
    }

    void move() {

        switch (direction) {
            case RIGHT:
                xSpeed = 10;
                ySpeed = 0;
                break;
            case LEFT:
                xSpeed = -10;
                ySpeed = 0;
                break;
            case DOWN:
                xSpeed = 0;
                ySpeed = 10;
                break;
            case UP:
                xSpeed = 0;
                ySpeed = -10;
                break;
        }

        headX += xSpeed;
        headY += ySpeed;

        if (headX <= 0)
            headX = 500;
        else if (headX > 500)
            headX = 0;

        if (headY <= 0)
            headY = 400;
        else if (headY > 400)
            headY = 0;

        head.setTail(headX, headY);
    }

    Tail getHead() {
        return head;
    }

    void addTail(int tailX, int tailY) {
        ++snakeLength;
        tail[snakeLength] = new Tail(tailX, tailY);
    }

    boolean snakeEatsTail() {
        boolean eatsTail = false;
        for (int i = 1; i <= snakeLength; i++) {
            if (head.getX() == tail[i].getX() && (head.getY() == tail[i].getY())) {
                eatsTail = true;
            }
        }
        return eatsTail;
    }

    int getSnakeLength() {
        return snakeLength;
    }

    class Tail {
        private int x, y;

        void setTail(int x, int y) {
            this.x = x;
            this.y = y;
        }

        int getX() {
            return x;
        }

        int getY() {
            return y;
        }

        Tail(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
