import java.applet.Applet;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

public class Game extends Applet implements Runnable, KeyListener {

    private Graphics graphics;
    private Image image;
    private Thread thread;
    private final int WIDTH = 500, HEIGHT = 400;
    private Snake snake;
    private Fruit fruit;
    private Random generator = new Random();
    private int snakeHeadX, snakeHeadY, level = 2;
    private boolean isRunning, isSettingName;
    private User user = new User("default", 0);
    StringBuilder sb;

    ArrayList<Character> letters;

    @Override
    public void init() {
        this.resize(WIDTH, HEIGHT);
        this.addKeyListener(this);

        image = createImage(WIDTH, HEIGHT);
        graphics = image.getGraphics();

        snake = new Snake();
        fruit = new Fruit();
        thread = new Thread(this);

    }

    @Override
    public void paint(Graphics g) {
        graphics.setColor(Color.GRAY);
        graphics.fillRect(0, 0, WIDTH, HEIGHT);
        if (thread.getState() == Thread.State.NEW) {
            graphics.setColor(Color.GREEN);
            graphics.setFont(new Font("TimesRoman", Font.PLAIN, 30));
            graphics.drawString("NACIŚNIJ SPACJĘ", 115, 200);
            graphics.setFont(new Font("TimesRoman", Font.PLAIN, 20));
        } else if (!isRunning) {
            graphics.setColor(Color.GREEN);
            graphics.setFont(new Font("TimesRoman", Font.PLAIN, 50));
            graphics.drawString("KONIEC GRY", 100, 200);
            graphics.setFont(new Font("Calibri", Font.PLAIN, 20));
            graphics.drawString("Wynik: " + snake.getSnakeLength(), 200, 280);
            if (isSettingName) {
                graphics.drawString(sb.toString(), 200, 320);
                System.out.println(sb.toString() + "   " + sb.length() + "          " + sb.toString().length());
            }
        } else {
            snake.draw(graphics);
            fruit.draw(graphics);
        }
        g.drawImage(image, 0, 0, this);
    }

    @Override
    public void update(Graphics g) {
        paint(g);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (isSettingName) {
            if (sb.length() < 10) sb.append(e.getKeyChar());
            repaint();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {

        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                if (snake.getDirection() != Snake.Direction.DOWN) {
                    snake.setDirection(Snake.Direction.UP);
                }
                break;
            case KeyEvent.VK_DOWN:
                if (snake.getDirection() != Snake.Direction.UP) {
                    snake.setDirection(Snake.Direction.DOWN);
                }
                break;
            case KeyEvent.VK_LEFT:
                if (snake.getDirection() != Snake.Direction.RIGHT) {
                    snake.setDirection(Snake.Direction.LEFT);
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (snake.getDirection() != Snake.Direction.LEFT) {
                    snake.setDirection(Snake.Direction.RIGHT);
                }
                break;
            case KeyEvent.VK_SPACE:
                if (!thread.isAlive() && !isRunning) {
                    try {
                        thread.start();
                    } catch (IllegalThreadStateException exception) {
                        exception.printStackTrace();
                    }
                }
                break;

            case KeyEvent.VK_ENTER:
                isSettingName = false;
                user.setName(sb.toString());
                System.out.println(sb.toString());
                break;
            case KeyEvent.VK_ESCAPE:
                System.exit(0);
                break;
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void run() {
        isRunning = true;
        while (isRunning) {

            snake.move();
            snakeHeadX = snake.getHead().getX();
            snakeHeadY = snake.getHead().getY();

            if (snake.snakeEatsTail()) {
                sb = new StringBuilder();
                isSettingName = true;
                isRunning = false;
                System.out.println("ZEZARL OGON");
            }

            if (fruit.checkCollision(snakeHeadX, snakeHeadY, fruit.getFruitX(), fruit.getFruitY())) {
                updateFruit();
            }

            repaint();
            try {
                Thread.sleep(100 / level);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateFruit() {
        snake.addTail(fruit.getFruitX(), fruit.getFruitY());
        fruit.setFruitX(10 * (generator.nextInt(49) + 1));
        fruit.setFruitY(10 * (generator.nextInt(39) + 1));
    }

// TODO create database with best scores

}

