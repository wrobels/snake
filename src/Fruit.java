import java.applet.Applet;
import java.awt.*;
import java.util.Random;

public class Fruit extends Applet {

    private int fruitX, fruitY;
    private Random generator = new Random();

    public Fruit() throws HeadlessException {
        fruitX = 10 * (generator.nextInt(49) + 1);
        fruitY = 10 * (generator.nextInt(39) + 1);
    }

    void setFruitX(int value) {
        fruitX = value;
    }

    int getFruitX() {
        return fruitX;
    }

    void setFruitY(int value) {
        fruitY = value;
    }

    int getFruitY() {
        return fruitY;
    }

    void draw(Graphics g) {
        g.setColor(Color.red);
        //  g.fillRect(fruitX - 10, fruitY - 10, 10, 10);
        g.fillOval(fruitX - 10, fruitY - 10, 10, 10);
    }

    boolean checkCollision(int headX, int headY, int fruitX, int fruitY) {
        return (headX == fruitX) && (headY == fruitY);
    }
}

